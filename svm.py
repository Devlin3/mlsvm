import os, random
import numpy as np
from sklearn import svm, preprocessing, metrics

random.seed(200)

#parses csv into two dimensional numpy array
def getData(fp, randomize = True):
    table = []
    with open(fp) as fh:
        for line in fh:
            line = line.strip()
            table.append(line.split(','))
    
    if randomize:
        random.shuffle(table)
    
    return np.array(table, dtype=np.float64)

#split data into training and testing sets
def splitData(table, expIndex = -1):
    #split data into two sets
    half = len(table)/2
    trainData = table[:half]
    testData = table[half:]
    
    #get column that has the classification
    testExp = testData[:,expIndex]
    trainExp = trainData[:,expIndex]
    
    #replace 0s with -1
    testExp = [x if x == 1 else -1 for x in testExp]
    trainExp = [x if x == 1 else -1 for x in trainExp]
    
    assert(len(testData[0]) == 58)
    #remove column that has classification. Only features are left
    testData = np.delete(testData, expIndex, axis=1)
    trainData = np.delete(trainData, expIndex, axis=1)
    assert(len(testData[0]) == 57)
    
    return testData, testExp, trainData, trainExp

def runExp(clf, testF, testE, roc, verbose = False):
    ret = clf.predict(testF)
    correct = 0
    tp = 0
    fp = 0
    fn = 0
    for result, expected in zip(ret, testE):
        if result == expected:
            correct += 1
        if result == 1 and expected == 1:
            tp += 1
        elif result == 1 and expected == -1:
            fp += 1
        elif result == -1 and expected == 1:
            fn += 1
    acuracy = float(correct)/ len(testE)
    if verbose:
        print("Accuracy: " + str(acuracy))
        print("Recall: " + str(float(tp)/(tp + fn + .00000000000000000000000001)))
        print("Percision: " + str(float(tp)/(tp + fp + .0000000000000000000000000001)))
   
    if roc:
        scores = clf.decision_function(testF)
        fpr, tpr, thresholds = metrics.roc_curve(testE, scores)

        for fp, tp, threshold in zip(fpr, tpr, thresholds):
            print(str(threshold) + "," + str(tp) + "," + str(fp))
        print(len(thresholds))

    return acuracy

if __name__ == "__main__":
    #data file is in same folder as script
    dataPath = os.path.join(os.path.dirname(os.path.realpath(__file__)), "spambase.data")
    allData = getData(dataPath)

    testFeatures, testExp, trainFeatures, trainExp = splitData(allData)
    
    scaler = preprocessing.StandardScaler(copy=True, with_mean=True, with_std=True)
    #scale based on test data
    trainFeatures = scaler.fit_transform(trainFeatures)
    #apply same scaling to test Features
    testFeatures = scaler.transform(testFeatures)

    clf = svm.SVC(kernel='linear')
    clf.fit(trainFeatures, trainExp)
    runExp(clf, testFeatures, testExp, True, True)
    
    print(clf.coef_)
    weightVector = clf.coef_[0]
    
    for testType in ["Best", "Random"]:
        print(testType)
        for i in range(2,len(trainFeatures[0]) + 1):
            
            #only consider n features
            bestInd = []
            if testType == "Random":
                bestInd = random.sample(range(0,len(trainFeatures[0])), i)
            else:
                bestInd = np.argsort(weightVector)[-i:]

            #only consider columns that are most valued
            testF = testFeatures[:, bestInd]
            trainF = trainFeatures[:, bestInd]
            
            clf = svm.SVC(kernel='linear')
            clf.fit(trainF, trainExp)
            accuracy = runExp(clf, testF, testExp, False, False)

            print(str(i) + "," + str(accuracy))

